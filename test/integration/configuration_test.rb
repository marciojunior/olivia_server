#encoding: UTF-8

require 'test_helper'

class ConfigurationTest < ActionDispatch::IntegrationTest

  test "Initial page layout" do

    assert_main_page_buttons
    assert_transition_to_server_form
    assert_save_server_data
    assert_transition_to_login_form
    assert_save_login_data

  end

  def assert_main_page_buttons
    visit "/index.html"
    find_button("Clientes").visible?
    find_button("Pedidos").visible?
    find_button("Sincronizar").visible?
    find_link("Consultas").visible?
  end

  def assert_transition_to_server_form
    find_button('configuration-button').click
    page.should have_content "Endereço servidor"
    fill_in 'servidor', with: 'http://kbo.no-ip.org:8090/forcavendas2/handler.do'
    all('.close').first.click
  end
  
  def assert_save_server_data
    sleep 2
    find_button('configuration-button').click
    page.should have_content "Endereço servidor"
    find('[name=servidor]').value.should == 'http://kbo.no-ip.org:8090/forcavendas2/handler.do'
    all('.close').first.click
  end

  def assert_transition_to_login_form
    sleep 2
    find_button('login-button').click
    page.should have_content "Login"
    fill_in 'codigoEmpresa', with: '1'
    fill_in 'login', with: '17'
    fill_in 'senha', with: 'LULA123'
    all('.close').first.click
    # binding.pry
  end

  def assert_save_login_data
    sleep 2
    find_button('login-button').click
    page.should have_content "Login"
    find('[name=codigoEmpresa]').value.should == "1"
    find('[name=login]').value.should == "17"
    all('.close').first.click
  end

  # helpers
  def click_dropdown_item
    sleep 2
    binding.pry
    all(".icon-check").first.click
  end

  def click_tab_link id
    page.evaluate_script "$('body').scrollTop(0);"
    find("a[href='\##{id}']").click
    sleep 1
  end

  def click_plus_link
    all("a .icon-plus", visible: true).first.click
    sleep 1
  end

end
