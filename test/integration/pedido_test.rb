#encoding: UTF-8

require 'test_helper'

class PedidoTest < ActionDispatch::IntegrationTest

  test "Initial page layout" do

    assert_main_page_buttons
    assert_transition_to_order_index
    assert_order_form_validation
    fill_order_form

  end

  def assert_main_page_buttons
    visit "/index.html"
    find_button("Clientes").visible?
    find_button("Pedidos").visible?
    find_button("Sincronizar").visible?
    find_link("Consultas").visible?
  end

  def assert_transition_to_order_index
    click_button "Pedidos"
    page.should have_content "Cadastro de pedidos"
  end

  def assert_order_form_validation
    # click_link("Novo") nao funciona
    find(".btn.btn-primary").click
    assert has_content? "Criar pedido"
    #click_link "Criar"
    all(".btn").select { |a| a.text == "Criar" }.first.click
    sleep 1
    alert = page.driver.browser.switch_to.alert
    alert.text.should == "Problema com preencimento dos dados. Verifique:\nCliente não informado.\nClassificação não informada.\nCondição de pagamento não informada.\nEndereço de cobrança não informado.\nEndereço de entrega não informado.\nDesconto da duplicata não informado."
    alert.dismiss
  end

  def fill_order_form
    fill_order_tab_one
  end

  def fill_order_tab_one
    sleep 1
    find_link('customer-select').click
    sleep 3
    binding.pry
    find_link('unsynced_customers').click
    sleep 2
    click_dropdown_item
    
    within '#tabelaPrecoSelect' do
      all('option').last.click
    end 

    fill_in "dataEmisVenda", with: Time.new(2013, 4, 5).strftime("%d/%m/%Y")
    fill_in "dataEntrega", with: Time.new(2013, 4, 6).strftime("%d/%m/%Y")

    within '#condicaoPagamentoSelect' do
      all('option').last.click
    end 

    
    fill_in "percDescDuplic", with: "17"
    fill_in "descontoTransient1", with: "10"
    fill_in "descontoTransient2", with: "12"
    fill_in "descontoTransient3", with: "5"
    
    within '#depositoSelect' do
      all('option').last.click
    end 


    all(".btn").select { |a| a.text == "Criar" }.first.click
    sleep 1
    alert = page.driver.browser.switch_to.alert
    alert.text.should == "Problema com preencimento dos dados. Verifique:\nCliente não informado.\nClassificação não informada.\nCondição de pagamento não informada.\nEndereço de cobrança não informado.\nEndereço de entrega não informado.\nDesconto da duplicata não informado."
    alert.dismiss
  end

  # helpers
  def click_dropdown_item
    sleep 2
    all(".icon-check").first.click
  end

  def click_tab_link id
    page.evaluate_script "$('body').scrollTop(0);"
    find("a[href='\##{id}']").click
    sleep 1
  end

  def click_plus_link
    all("a .icon-plus", visible: true).first.click
    sleep 1
  end

end
