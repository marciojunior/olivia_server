#encoding: UTF-8

require 'test_helper'

class ClienteTest < ActionDispatch::IntegrationTest

  test "Initial page layout" do

    assert_main_page_buttons
    assert_transition_to_customer_index
    assert_customer_form_validation
    fill_customer_form
    save_customer

  end

  def assert_main_page_buttons
    visit "/index.html"
    page.execute_script("Olivia.Seeds.process()")
    sleep 4
    find_button("Clientes").visible?
    find_button("Pedidos").visible?
    find_button("Sincronizar").visible?
    find_link("Consultas").visible?
  end

  def assert_transition_to_customer_index
    click_button "Clientes"
    assert has_content? "Cadastro de clientes"
  end

  def assert_customer_form_validation
    # click_link("Novo") nao funciona
    find(".btn.btn-primary").click
    assert has_content? "Criar cliente"
    #click_link "Criar"
    all(".btn").select { |a| a.text == "Criar" }.first.click
    sleep 1
    alert = page.driver.browser.switch_to.alert
    alert.text.should == "Houveram erros na validação dos seguintes campos:\nCampo cnpj: O cnpj do cliente está inválido" 
    alert.dismiss
  end

  def fill_customer_form
    fill_customer_tab_one
    fill_customer_tab_two
    fill_customer_tab_three
    fill_customer_tab_four
    fill_customer_tab_five
    fill_customer_tab_six
  end

  def fill_customer_tab_one
    fill_in "cnpj9", with: "1"
    fill_in "cnpj4", with: "2"
    fill_in "cnpj2", with: "17"
    fill_in "nomeCliente", with: "Teste razao"
    fill_in "fantasiaCliente", with: "Teste fantasia"
    fill_in "inscEstCliente", with: "Teste inscricao cliente"
    fill_in "cepEntrCobr", with: "89255800"
    fill_in "endEntrCobr", with: "Rua jose scheuer"
    fill_in "numeroImovel", with: "12"
    fill_in "complemento", with: "Perto do buteco"
    fill_in "bairroEntrCobr", with: "Amizade"
    click_link "choose-customer-city"

    click_dropdown_item
    fill_in "cxpostalCliente", with: "123456"
    fill_in "telefoneCliente", with: "11235813"
    fill_in "faxCliente", with: "987654321"
    fill_in "eMail", with: "marciojunior_eu@yahoo.com.br"
    fill_in "nfeEmail", with: "marciojunior_eu@nfe.com.br"
    #select "", from: "grupoEconomico"
    click_link "choose-customer-carrier"
    click_dropdown_item
    fill_in "informacao", with: "lorem ipsum dolor amet"
  end

  def fill_customer_tab_two
    click_tab_link "cliente-tab-two"
    assert has_content? "Outros endereços"

    click_plus_link
    assert has_content? "Cadastro de endereço"

    within ".modal.in" do
      fill_in "endEntrCobr", with: "Rua Irmão Leandro"
      fill_in "numeroImovel", with: "13"
      fill_in "complemento", with: "Perto do supermecado souza"
      fill_in "bairroEntrCobr", with: "Tifa Martins"
      click_link "choose-customer-address-city"
    end
    within "#cidades-list-modal" do
      click_dropdown_item
    end

    within ".modal.in" do
      fill_in "cepEntrCobr", with: "8912345"
      fill_in "caixaPostal", with: "43243223"
      click_button "Entrega"
    end
    click_link "Salvar"

    sleep 1

    #save_screenshot "/home/marcio/file.png", full: true
    # TODO essa porra do inferno nao funciona
    click_plus_link
    assert has_content? "Cadastro de endereço"


    within ".modal.in" do
      fill_in "endEntrCobr", with: "Rua José Narloch"
      fill_in "numeroImovel", with: "23"
      fill_in "complemento", with: "Perto da grafipel"
      fill_in "bairroEntrCobr", with: "Tifa Martins"
      click_link "choose-customer-address-city"
    end

    within "#cidades-list-modal" do
      click_dropdown_item
    end

    within ".modal.in" do
      fill_in "cepEntrCobr", with: "34233"
      fill_in "caixaPostal", with: "9998877"
      click_button "Cobrança"
    end

    click_link "Salvar"
  end

  def fill_customer_tab_three
    click_tab_link "cliente-tab-three"
    sleep 1
    assert has_content? "Informações da empresa:"
    fill_in "numeroRegJunta", with: "1"
    fill_in "dataFundacao", with: Time.new.strftime("%d/%m/%Y")
    fill_in "numeroFiliais", with: "3"
    fill_in "capitalAtual", with: "123.45"
    fill_in "valorFaturamentoAnual", with: "456.78"
    click_button "Não"
    fill_in "sugLimiteMaxPed", with: "21"
    # TODO esse nao eh o dado oficial
    select "Foo", from: "tipoCliente"
    select "Bar", from: "segmentoMercado"
  end

  def fill_customer_tab_four
    click_tab_link "cliente-tab-four"
    assert has_content? "Preferência bancária"
    click_button "Bradesco"
    click_button "Safra"
    click_plus_link
    assert has_content? "Consulta de Bancos"

    within ".modal-body" do
      click_button "CEF"
      click_button "Banco do Brasil"
      fill_in "agenciaBanco", with: "1"
    end
    click_link "Salvar" # bug ao fechar modal
  end

  def fill_customer_tab_five
    sleep 1
    click_tab_link "cliente-tab-five"
    assert has_content? "Referências Comerciais"
    click_plus_link
    assert has_content? "Cadastro de referência comercial"
    fill_in "nome", with: "Supermecado souza"
    fill_in "telefone", with: "33701122"
    click_link "Salvar" # bug ao fechar modal
  end

  def fill_customer_tab_six
    sleep 1
    click_tab_link "cliente-tab-six"
    assert has_content? "Sócios/Diretores"
    click_plus_link
    assert has_content? "Cadastro de sócio"
    fill_in "cpf9", with: "078243669"
    fill_in "cpf2", with: "28"
    fill_in "nomeContato", with: "Marcio Junior"
    fill_in "nomeFuncao", with: "Progamador pessoa nas horas vagas"
    fill_in "dataNascimento", with: Time.new(1991, 5, 5).strftime("%d/%m/%Y")
    fill_in "percParticipacao", with: "95.5"
    fill_in "email", with: "marciojunior_eu@yahoo.com.br"
    find(:xpath, "(//a[text()='Salvar'])",visible:true).click
  end

  def save_customer
    click_tab_link "cliente-tab-main"
    all(".btn").select { |a| a.text == "Criar" }.first.click
    sleep 1
    assert has_content? "Cadastro de clientes"
    page.all('table.table-condensed tr').count.should == 2
  end
  
  # helpers
  def click_dropdown_item
    sleep 2
    all(".icon-check").first.click
  end

  def click_tab_link id
    page.evaluate_script "$('body').scrollTop(0);"
    find("a[href='\##{id}']").click
    sleep 1
  end

  def click_plus_link
    all("a .icon-plus", visible: true).first.click
    sleep 1
  end

end
