ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'rspec/rails'
require 'capybara/dsl'
require 'capybara/rails'
require 'capybara/rspec'
# require 'capybara/poltergeist'
# require 'capybara/webkit'



class ActiveSupport::TestCase
  #ActiveRecord::Migration.check_pending!

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  #fixtures :all

  # Add more helper methods to be used by all tests here...
end

class ActionDispatch::IntegrationTest
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL

  RSpec.configure do |config|
    config.include Capybara::DSL, :type => :request
    config.include Capybara::RSpecMatchers, :type => :request

    config.treat_symbols_as_metadata_keys_with_true_values = true
    config.filter_run :focus => true
    config.run_all_when_everything_filtered = true
  end

  # Stop ActiveRecord from wrapping tests in transactions
  #self.use_transactional_fixtures = false

  Capybara.current_driver = :selenium
  Capybara.javascript_driver = :selenium_chrome
  Capybara.run_server = false
  Capybara.app_host = 'http://localhost:3000/olivia'

  #Capybara.current_driver = :poltergeist

  #Capybara.javascript_driver = :webkit
  #Capybara.default_driver = :webkit

  #Capybara.default_driver = :poltergeist

  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end


  teardown do

    # Capybara.reset_sessions!    # Forget the (simulated) browser state
    # Capybara.use_default_driver # Revert Capybara.current_driver to Capybara.default_driver
  end
end