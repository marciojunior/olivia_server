# Be sure to restart your server when you modify this file.

# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
OliviaServer::Application.config.secret_key_base = 'e71fb30c2b3fb91c6598b795f0aa34a061749224afcfa9411efc6a363a6bda01124b99f8c46a4bfaba39a587a3323ce438748d23fa2730831de17b8fd5d4a76a'
