# Be sure to restart your server when you modify this file.

OliviaServer::Application.config.session_store :encrypted_cookie_store, key: '_olivia_server_session'
